/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author USER
 */
class lab_3 {

    public static boolean checkWin(String[][] table, String currentplayer) {
        return checkRows(table, currentplayer) || checkColumns(table, currentplayer) || checkDiagonals(table, currentplayer);
    }

    private static boolean checkRows(String[][] table, String currentplayer) {
        for (int row = 0; row < 3; row++) {
            if (table[row][0].equals(currentplayer) && table[row][1].equals(currentplayer) && table[row][2].equals(currentplayer)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkColumns(String[][] table, String currentplayer) {
        for (int col = 0; col < 3; col++) {
            if (table[0][col].equals(currentplayer) && table[1][col].equals(currentplayer) && table[2][col].equals(currentplayer)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagonals(String[][] table, String currentplayer) {
        if (table[0][0].equals(currentplayer) && table[1][1].equals(currentplayer) && table[2][2].equals(currentplayer)) {
            return true;
        }
        if (table[0][2].equals(currentplayer) && table[1][1].equals(currentplayer) && table[2][0].equals(currentplayer)) {
            return true;
        }
        return false;
    }
    public static boolean checkDraw(String[][] table) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col].equals("-")) {
                    return false;
                }
            }
        }
        return true;
    }

    static int add(int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
